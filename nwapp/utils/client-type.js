'use strict';

const os = require('os');

module.exports = (function () {
  const platform = os.platform();
  if (platform.match(/darwin/)) return 'osx';
  if (platform.match(/^win/)) return 'win';
  if (platform.match(/linux/)) return 'linux';
  return os;
})();
